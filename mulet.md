---
title: Le Festival du mulet est en Belgique
subtitle: C'est le rendez-vous des bobos trolls fan de Salut c'est cool
---

Des centaines de fans du "mulet" ont participé samedi en Belgique à un festival dédié à cette coupe de cheveux d'un autre temps. 

Nostalgiques des années 1980 ou amateurs d'autodérision contents de défier la mode : des centaines de fans du "mulet" ont participé samedi en Belgique à un festival dédié à cette coupe de cheveux associant tempes dégagées et longueur dans la nuque. Pour certains, elle rappelle le rockeur Rod Stewart, pour d'autres MacGyver, le héros de la série télévisée éponyme.

## Cette coupe c'est un état d'esprit, une déclaration d'indépendance

![Les mulets ne sont pas sexy. Il faut arrêter](/coupe_mulet.jpg)

Le principe est surtout de laisser libre cours à "la dewanne", le délire ou la déconne en patois du Borinage, un coin de la campagne wallonne digne du "far-west belge", selon Damien Hubert, l'un des organisateurs. C'est sur un terrain de la brasserie artisanale dont il est cofondateur, à Boussu, non loin de Mons, que se tenait le festival à l'humour "15e degré" complètement assumé. Avec ses stands de coiffeurs accueillant les courageux prêts à franchir le pas.

> Cette coupe c'est un état d'esprit, une déclaration d'indépendance (...) la charge symbolique c'est vraiment une affirmation de soi - Damien Hubert

L'idée du festival était venue d'un groupe d'amis musiciens ayant décidé de se "tailler le mulet" pour un clip. "Pour être très franc, je ne suis pas sûr qu'il y ait beaucoup de gens qui aient jamais trouvé ça très très joli. On est clair là-dessus", a-t-il fait remarquer.

"Une première en Europe"
Les organisateurs ont revendiqué quelque 1.500 participants pour ce qui était "une première en Europe". En 2018, un festival similaire avait été organisé en Australie.

Beaucoup de visiteurs à Boussu étaient des trentenaires ou quadragénaires nourris aux séries télé, concerts ou matchs de football des années 1980-90, époque phare de la coupe mulet. "Quand j'étais petite, j'étais amoureuse de MacGyver, là je retourne en enfance", a plaisanté Marie Vandeville, 31 ans, entre les mains du coiffeur. Mais l'expérience du mulet ne va durer que quelques jours, assure-t-elle, "après on ré-égalisera".