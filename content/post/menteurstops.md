--- 
title: Les menteries de Moncrabeau 
---

Ça bosse dur chez Dominique Liégeois, retraité de 59 ans qui coule, d'habitude, des jours paisibles dans le placide Namur. Le premier dimanche d'août, il va défendre d'arrache-pied, face à des arracheurs de dents, son titre de… roi des menteurs. 

Il l'a brillamment décroché l'année dernière, en détrônant la Néracaise Marie-Laure Fornaro, devant plus de 200 personnes, et à plus de 1.000 km de chez lui, à Moncrabeau, petit village au cœur de la touffeur du Lot-et-Garonne où a lieu chaque année le Festival international de menteries. 

Oui, oui, international puisque, outre le Belge Dominique, il y a eu aussi des rois québécois ou allemands et des candidats venus de Colombie, des Etats-Unis, de Nouvelle-Zélande et d'Espagne ! « J'ai gagné en racontant des anecdotes sur les malentendus que génère mon prénom épicène. 

J'avais mélangé du vrai, comme la fois où j'ai gagné un vélo pour femme, à du faux, à savoir une convocation à une mammographie où quand le médecin m'a vu et m'a proposé à la place une testiculographie. Là, j'ai bien entendu aux rires que j'avais emporté la partie ! » se souvient l'ancien gendarme avec une légitime fierté. 

Du coup, pour cette année, il polit sa nouvelle menterie (autour d'anecdotes de sa carrière dans la gendarmerie, mais… chut), la peaufine et l'apprend par cœur… « Une bonne menterie doit être déclamée - et non lue - avec finesse, faire rire, ne porter tort à personne, ne pas parler de politique ou de religion, et tenir en haleine le public.

Enfin, elle ne doit pas dépasser les six minutes », explique doctement Denis Delfour, vice-président de l'Académie des menteurs, auguste association à but non lucratif qui organise le festival depuis 1972. 

![on vous a déjà dit que c'était sympa de mentir ?](/Menteurs_moncrabeau.jpg)

## TRAVESTIR LA VÉRITÉ

Le déroulé du concours reste immuable. Après une foire aux produits du terroir et des danses traditionnelles gasconnes, à 15 h tapantes, les 40 académiciennes et académiciens, sanglés dans un costume pourpre façon robe de Richelieu (pour homme) ou costume provençal (pour femme), s'installent, sous le cagnard et sur la place du petit marché en compagnie de la foule des curieux. 

La petite dizaine de prétendants au titre se rendent alors sur le fauteuil des menteurs, modeste trône de pierre de taille, scellé dans un mur sous la Pierre de vérité (c'est gravé dessus ainsi que la date de 1748). Ils prêtent serment « de travestir la vérité, toute la vérité, rien que la vérité » puis ils racontent leur bobard pour le parterre rigolard des habitants du village et des touristes trop heureux d'assister à ce moment pas banal. 

Pour juger du meilleur mensonge, c'est-à-dire à la fois le plus drôle et le plus crédible, pas de vote à main levée (générateur d'embrouilles) ou de points façon Eurovision (trop compliqué). À la fin de l'histoire, chaque académicien vote avec des cuillères de sel recueillies dans un sac par des petits pages. Plus il a aimé l'histoire, plus il verse de cuillères dans le sac. 

Celui-ci est pesé - au gramme près - par un « ingénieur des poids et mesures » sur une antique balance à trébuchet et le plus lourd désigne le gagnant du concours. Simple, basique. Le roi (ou la reine) est alors trimbalé dans un siège à porteurs dans les petites rues du bourg entre les façades de pierres blanches et sous les hourras des habitants. 

« Tout ce cérémonial est pris très au sérieux, c'est ça qui est drôle », raconte, goguenard, Dominique Liégeois. Outre le titre envié, le gagnant repart avec une coupe, une bouteille d'armagnac et un « brevet de menteur » lui octroyant le droit de dire n'importe quoi durant l'année. Comme dans les albums d'Astérix, tout se termine par un banquet au marché gourmand installé dans le village.

