---
Title = "FLOP : City Trucks ou le Paradis des camionneurs"
Image = "/static/citytrucks.jpg"
---
Ah, le tire-camion… Imaginez huit forcenés tirant comme des malades sur une corde en chanvre d'environ 4 cm de diamètre. A l'autre bout, un bahut rutilant de 16 t. Le but ? Faire parcourir 6 mètres en un minimum de temps à la bête. Le tout sous 35 °C, bien sûr. L'an dernier, 880 masochistes se sont livrés à l'exercice.

Jusqu'à ce qu'on découvre l'existence de ce festival, on croyait naïvement que, dans un festival de musique, le chanvre était principalement destiné à être fumé, enroulé dans un classique deux feuilles. On vous rassure, ici c'est aussi le cas.

###GROS CAMION ET GROSSES ENCEINTES

Mais, sur le City Trucks, la fumée est principalement produite par les dizaines de camions disséminés sur le site. Dès l'arrivée, le visiteur est accueilli par une haie d'honneur de 70 gros culs. Ils vous diraient volontiers bonjour, mais les sirènes sont interdites pendant les concerts. Sponsors et partenaires de l'événement, un nombre équivalent de transporteurs y participent via des stands de recrutement et d'information. Hotchkiss, Krieger, Ford, Mack… Des véhicules de collection sont aussi exposés.

On comprendra aisément que vous n'en ayez rien à carrer, mais c'est faire l'impasse sur une vérité absolue énoncée par feu Max Meynier sur RTL il y a quarante ans : les routiers sont sympas. Et que font ces chauffeurs une fois leur destrier garé à côté des tour bus des stars du rock ? Ils s'égaillent dans le festival, marcel moulant, tatouages de dauphin et gitane au bec. Désolé… On a foncé tout droit dans les clichés. C'est précisément ces idées reçues que David Dubillot, cofondateur de la manifestation avec sa femme Blandine, veut éviter. « Si on a créé City Trucks, c'est aussi pour battre en brèche les représentations que l'on peut se faire de cet univers. Le cliché du gars bourru en débardeur, faut arrêter ! Ça a complètement changé. Aujourd'hui, c'est très réglementé. Les gars ont un GPS et ne partent plus trois semaines d'affilée loin de leur famille. » Et la CB (éméteur-récepteur radio prisé des routiers) ? « Il n'y a plus personne sur le réseau. » Et le gros rouge des relais routiers ? « Les chauffeurs sont obligés de souffler dans un éthylotest avant de démarrer. » Et la sirène ? « Quand on se croise, on le fait toujours. » Ouf.

Si David, 36 ans, sait tout ça, c'est qu'il a été de la maison pendant quinze ans. CAP conducteur routier marchandises à Châteaubriant (Loire-Atlantique), premier contrat à 18 ans. Avec son 44 t il a traversé la France dans tous les sens, emmenant à l'occasion sa Blandine (fille de routier), pour partir en week-end aux Pays-Bas. Il a même transporté le tapis de sol recouvrant le gazon du Stade de France pour un concert de Paul McCartney. Mais David n'était pas un camionneur comme les autres : il était DJ routier.

###AVEC SHAKA PONK, BIGFLO ET OLI

Non, il ne s'amusait pas à mixer en conduisant, mais, les congés venus, il abandonnait son volant pour parcourir les Pays de la Loire avec sa disco mobile, le New Delir Sono. Matmatah, Louise attaque, Elmer Food Beat… Il avait déjà une idée fixe : « Je n'aime pas que les gens restent assis. » Vers 2010, le couple décide de lâcher fourneaux et bahut pour se lancer dans la disco mobile à l'allemande, avec chapiteaux à étages. L'affaire est trop compliquée et David retrouve le bitume. A la fête de sa boîte, en 2014, à La Pommeraye (Maine-et-Loire), il est chargé d'organiser un pique-nique pour 220 salariés. Pilotages de Ferrari et Lamborghini, feux d'artifice, balades en calèche, manèges, simulateurs de poids lourd… Il se découvre une vocation de Barnum qui éveille la curiosité de la presse locale. « Ils nous ont dit que ce serait bien de le refaire, mais pour tout le monde. En rigolant on a dit : “Revenez dans deux ans, ce sera 100 fois plus gros.” »

En 2016, pari tenu : aidé par des boîtes de transport locales, City Trucks est né. Lors de sa première édition, devant 20 000 festivaliers, il accueille Boulevard des Airs, Michael Jones et… Louise attaque.

Depuis Shaka Ponk, Bigflo et Oli ou Les Négresses vertes ont suivi. Cette année étaient annoncés sur les deux scènes : Orelsan, Cali, No One Is Innocent et Elmer Food Beat, évidemment. L'édition 2019, prévue à partir du 30 août prochain, a dû être annulée pour raisons financières.

Pourquoi c'est un flop : Culte de gros engins polluants à l'heure de l'urgence écologique (#Balancetonroutier), immixion capitaliste et relents machistes s'additionnent dans un même constat : ce festival n'est absolument pas Charlie. Oui nous sommes des SJW.
